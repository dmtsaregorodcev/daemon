<?php

namespace Queue\Decorator;

use Queue\Client;

class BatchClient
{
    /**
     * @var int
     */
    private $size;
    private $items = [];

    public function __construct(Client $client, $size = 1000)
    {
        $this->client = $client;
        $this->size = $size;
    }

    public function send($content)
    {
        if(empty($content)){
            return;
        }
        if(count($this->items) === $this->size){
            $this->client->send($this->items);
            $this->items = [];
        }
        $this->items[] = $content;
    }
}