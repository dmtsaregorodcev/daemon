<?php

namespace Queue\Driver\File;
use Queue\Client;
use Queue\Converter\SerializeInterface;
use Queue\Driver\QueueInterface;

class Queue implements QueueInterface
{
    private $file;
    private $serialize;
    /**
     * @var Client
     */
    private $client;

    /**
     * Queue constructor.
     * @param string $file
     * @param SerializeInterface $serialize
     * @param $client
     */
    public function __construct($file, SerializeInterface $serialize,$client)
    {
        $this->file = $file;
        $this->serialize = $serialize;
        $this->client = $client;
    }

    public function run()
    {
        while(true){
            $this->data(function(&$content){
                $this->client->send($content);
                $content = [];
            });
            usleep(2);
        }

    }

    public function clear()
    {
        $this->data(function(&$content){
            $content = [];
        });
        unset($this->file);
    }

    public function size()
    {
        $size = 0;
        $this->data(function($content) use (&$size){
            $size = count($content);
        });
        return $size;
    }

    public function push($message)
    {
        $this->data(function(&$content) use ($message){
            $content[] = $message;
        });
    }

    /**
     * @param callable $callback
     * @throws \Exception
     */
    private function data(callable $callback)
    {
        if(!file_exists($this->file)){
            touch($this->file);
        }
        if (($file = fopen($this->file, 'rb+')) === false) {
            throw new \Exception("Ошибка открытия pid : $this->file");
        }
        flock($file, LOCK_EX);
        try{
            $content = stream_get_contents($file);
            $data =  $this->serialize->decode($content);
            $callback($data);
            $newData =  $this->serialize->encode($data);
            if($data !== $newData){
                ftruncate($file, 0);
                rewind($file);
                fwrite($file, $newData);
                fflush($file);
            }
        } finally {
            flock($file, LOCK_UN);
            fclose($file);
        }
    }
}