<?php

namespace Queue\Driver;


interface QueueInterface
{
    public function run();

    public function clear();

    public function size();

    public function push($message);
}