<?php

namespace Queue;


class PidFile
{
    private $pidFile;
    private $pid;

    public function __construct($pidFile)
    {
        $this->pidFile = $pidFile;
    }

    public static function build($file)
    {
        if(!file_exists($file)){
            touch($file);
        }
        return new self($file);
    }

    public function exist()
    {
        $pid = file_get_contents($this->pidFile);
        if (empty($pid) || posix_getsid($pid) === false) {
            file_put_contents($this->pidFile, getmypid());
            return false;
        }
        return true;
    }

    public function pid()
    {
        if($this->pid === null){
            $this->pid = file_get_contents($this->pidFile);
        }
        return $this->pid;
    }

    public function remove()
    {
        unlink($this->pidFile);
    }
}