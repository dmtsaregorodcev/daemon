<?php

namespace Queue\Converter;


interface SerializeInterface
{
    /**
     * @param $content
     * @return mixed
     */
    public function encode($content);

    /**
     * @param $content
     * @return string
     */
    public function decode($content);

}