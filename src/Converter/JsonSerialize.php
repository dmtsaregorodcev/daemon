<?php

namespace Queue\Converter;

class JsonSerialize implements SerializeInterface
{
    /**
     * @param $content
     * @return mixed
     */
    public function encode($content)
    {
        return json_encode($content);
    }

    /**
     * @param $content
     * @return string
     */
    public function decode($content)
    {
        return json_decode($content,true);
    }
}