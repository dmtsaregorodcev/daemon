<?php

namespace Queue;


class Router
{
    protected $items;

    public function __construct()
    {
        $this->items = [];
    }

    public function add($name,$handle)
    {
        $this->items[$name] = $handle;
    }

    public function get(array $argv)
    {
        if (empty($argv[1])) {
            return function(){
                fwrite(STDOUT, 'Ошибка в команде' . PHP_EOL);
                exit();
            };
        }
        $routeName = $argv[1];
        foreach ($this->items as $name => $handle){
            if($name === $routeName){
                return $handle;
            }
        }
        return function(){
            fwrite(STDOUT, 'События не существует' . PHP_EOL);
            exit();
        };
    }


}