<?php

namespace Queue;


class Application
{
    /**
     * @var Router
     */
    private $router;

    public function __construct(Router $router)
    {
        $this->router = $router;
    }

    public function route($name,$handler){
        $this->router->add($name,$handler);
    }

    public function run(array $argv)
    {
        $callback = $this->router->get($argv);
        $callback(\array_slice($argv,2));
    }
}