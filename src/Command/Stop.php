<?php

namespace Queue\Command;


use Queue\Driver\QueueInterface;
use Queue\PidFile;

class Stop
{

    /**
     * @var PidFile
     */
    private $pidFile;
    /**
     * @var QueueInterface
     */
    private $queue;

    public function __construct(PidFile $pidFile, QueueInterface $queue)
    {
        $this->pidFile = $pidFile;
        $this->queue = $queue;
    }

    public function __invoke()
    {
        if(!$this->pidFile->exist()){
            fwrite(STDOUT, 'Демон не запущен' . PHP_EOL);
            exit;
        }
        posix_kill($this->pidFile->pid(), SIGTERM);
        $this->pidFile->remove();
        $this->queue->clear();
    }
}