<?php

namespace Queue\Command;


use Queue\Driver\QueueInterface;
use Queue\PidFile;

class Status
{
    /**
     * @var PidFile
     */
    private $pidFile;
    /**
     * @var QueueInterface
     */
    private $queue;

    public function __construct(PidFile $pidFile, QueueInterface $queue)
    {

        $this->pidFile = $pidFile;
        $this->queue = $queue;
    }

    public function __invoke()
    {
        $pid = $this->pidFile->pid();
        if (!(exec("ps -p $pid| grep $pid -c") > 0)) {
            echo "Демон не запущен\n";
        } else {
            echo "Демон запущен\n";
            echo "pid:$pid\n";
            echo "Количество:" . $this->queue->size() . "\n";
        }
    }
}