<?php

namespace Queue\Command;


use Queue\Converter\SerializeInterface;
use Queue\Driver\QueueInterface;

class Send
{
    /**
     * @var QueueInterface
     */
    private $queue;
    /**
     * @var SerializeInterface
     */
    private $serialize;

    public function __construct(QueueInterface $queue,SerializeInterface $serialize)
    {
        $this->queue = $queue;
        $this->serialize = $serialize;
    }

    public function __invoke($argv)
    {
        $message = $this->serialize->decode('{"login":"username","text":"some random message"}');
        $this->queue->push($message);
    }
}