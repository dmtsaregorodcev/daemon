<?php

namespace Queue\Command;


use Queue\Driver\QueueInterface;
use Queue\PidFile;

class Start
{
    /**
     * @var PidFile
     */
    private $pidFile;
    /**
     * @var QueueInterface
     */
    private $queue;

    public function __construct(PidFile $pidFile,QueueInterface $queue)
    {
        $this->pidFile = $pidFile;
        $this->queue = $queue;
    }

    public function __invoke()
    {
        if ($this->pidFile->exist()) {
            fwrite(STDOUT, 'Демон уже запущен' . PHP_EOL);
            exit();
        }
        $this->queue->run();
    }
}