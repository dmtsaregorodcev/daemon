<?php
use Pimple\Container;
use Queue\Converter\JsonSerialize;
use Queue\Decorator\BatchClient;
use Queue\PidFile;

$container = new Container();
$container['app'] = function ($c) {
    return new \Queue\Application(new \Queue\Router());
};
$container['pidFile'] = function($c){
    return PidFile::build(__DIR__.'/daemon.loc');
};
$container['start'] = function ($c){
    return new Queue\Command\Start($c['pidFile'],$c['queue']);
};
$container['client'] = function ($c){
    return new BatchClient(new \Queue\Client(),3);
};
$container['queue'] = function ($c){
    return new \Queue\Driver\File\Queue(__DIR__.'/queue.data',$c['serialize'],$c['client']);
};
$container['serialize'] = function ($c){
  return new JsonSerialize();
};
$container['send'] = function ($c){
    return new \Queue\Command\Send($c['queue'],$c['serialize']);
};

$container['stop'] = function ($c){
    return new \Queue\Command\Stop($c['pidFile'],$c['queue']);
};

$container['status'] = function ($c){
    return new \Queue\Command\Status($c['pidFile'],$c['queue']);
};
return $container;
