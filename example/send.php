<?php
chdir(dirname(__DIR__));
require 'vendor/autoload.php';

$container = require 'container.php';
$app = $container['app'];
$app->route('start',$container['start']);
$app->route('stop',$container['stop']);
$app->route('status',$container['status']);
$app->route('send',$container['send']);
$app->route('errors',function(){

});

$app->run($argv);